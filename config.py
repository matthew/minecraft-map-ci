import os

texturepath = os.environ['CLIENT_PATH']

worlds['minecraft'] = "./world"

renders["day"] = {
    'world': 'minecraft',
    'title': 'Day',
    'rendermode': 'smooth_lighting',
    "dimension": "overworld"
}

outputdir = "public"
